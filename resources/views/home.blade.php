@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Experts App</div>

                    <div class="card-body">
                        @if (session('message'))
                            <div class="alert alert-success" role="alert">
                                {{ session('message') }}
                                your appointment will be on<br>
                                {{session('appointment')->date->format('d M y')}}<br>
                                from {{\Carbon\Carbon::parse(session('appointment')->start)->format('g:i A')}}
                                to {{\Carbon\Carbon::parse(session('appointment')->end)->format('g:i A')}}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-6">Ip Timezone :</div>
                            <div class="col-md-6">{{$data["ip_timezone"]}}</div>
                        </div>
                        <br><br>
                        <div class="row">
                            <a class="btn btn-info text-white" href="{{route('experts')}}">View Experts</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection