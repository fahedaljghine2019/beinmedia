<option disabled selected></option>
@foreach($session_slots as $slot)
    <option data-from="{{getTimeIn12($slot->from)}}" data-to="{{getTimeIn12($slot->to)}}" value="{{$slot->from}} - {{$slot->to}}">{!! $slot->text() !!}</option>
@endforeach