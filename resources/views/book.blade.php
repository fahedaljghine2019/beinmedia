@extends('layouts.app')
@section('style')
    <link href='{{asset('js/datepicker/css/bootstrap-datepicker.min.css')}}'
          rel='stylesheet'/>
    <link href='{{asset('js/datepicker/css/bootstrap-datepicker.standalone.min.css')}}'
          rel='stylesheet'/>
    <link href='{{asset('js/datepicker/css/bootstrap-datepicker3.min.css')}}'
          rel='stylesheet'/>
    <link href='{{asset('js/datepicker/css/bootstrap-datepicker3.standalone.min.css')}}'
          rel='stylesheet'/>
@stop
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Experts: {{$data["expert"]->name}}</div>

                    <div class="card-body">
                        <div class="row justify-content-center">
                            <form method="post" action="{{route('book')}}">
                                @csrf
                                <input type="hidden" name="expert_id" value="{{$data["expert"]->id}}">
                                <div class="col-md-12">
                                    <label for="calendar">Select date</label>
                                    <div id="calendar"></div>
                                    <input name="date" type="hidden" value="{{date('Y-m-d')}}">
                                </div>
                                <div class="col-md-12" style="margin-top: 30px;">
                                    <span>Timezone: {{getTimeZoneOffset(session()->get('timezone'))}} {{session()->get('timezone')}}</span>
                                </div>
                                <div class="col-md-12" style="margin-top: 30px;">
                                    <div class="form-group">
                                        <label for="duration">Duration</label>
                                        <select name="duration" class="form-control">
                                            <option disabled selected></option>
                                            @foreach($data["durations"] as $item)
                                                <option value="{{$item->duration}}">{{getDurationText($item->duration)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12" style="margin-top: 30px;">
                                    <div class="form-group">
                                        <label for="slot">Time slot</label>
                                        <select name="slot" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div style="display: none;" class="alert alert-info" id="message">

                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 30px;">
                                    <button type="submit" class="btn btn-dark">Submit</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="{{asset('js/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('js/moment.js')}}"></script>
    <script>
        $('#calendar').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd',
        }).on('changeDate', function (e) {
            let date = e.format();
            $('input[name=date]').val(date);
        });

        $(document).on('change', 'select[name=duration]', function () {
            let duration = $(this).val();
            let date = $('input[name=date]').val();

            $.get('{{route('slots',$data["expert"]->id)}}', {duration: duration, date: date}, function (data) {
                $('select[name=slot]').html(data);
            });
        });

        $(document).on('change', 'select[name=slot]', function () {
            let date = $('input[name=date]').val();
            let date_string = moment(date, "YYYY-MM-DD").format("DD MMMM YYYY");

            let message = "Your appointment will be on<br>";
            message += date_string;
            message += "<br>from " + $('select[name=slot]').find(':selected').data('from') + " to " + $('select[name=slot]').find(':selected').data('to');
            $("#message").html(message).show();
        });
    </script>
@stop