@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Experts: {{$data["expert"]->name}}</div>

                    <div class="card-body">
                        <div class="row text-center">
                            <div class="col-md-12">
                                <img style=" border-radius: 50%;" width="100px;"
                                     src="{{asset('storage/' . $data["expert"]->image)}}">
                            </div>
                            <div class="col-md-12">
                                <h2 class="text-info">{{ $data["expert"]->name}}</h2>
                                <h4>{{ $data["expert"]->specialization->name}}</h4>
                            </div>
                            <div class="col-md-8 offset-2 justify-content-center" style="border: 3px solid black;">
                                <div class="col-md-12">
                                    <h5>Country : {{$data["expert"]->country->name}}</h5>
                                </div>
                                <div class="col-md-12" style="margin-top: 30px;">
                                    <h5>Working Hours : {{$data["expert"]->from}}  -> {{$data["expert"]->to}}</h5>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top: 30px;">
                                <a href="{{route('experts.book' , $data["expert"]->id)}}" class="btn btn-dark">Book now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop