@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Experts</div>

                    <div class="card-body">
                        <div class="row">
                            @php($i = 0)
                            @foreach($data["experts"] as $expert)
                                @php($i++)
                                <div class="col-md-6 text-center">
                                    <div class="col-md-12">
                                        <img style=" border-radius: 50%;" width="100px;"
                                             src="{{asset('storage/' . $expert->image)}}">
                                    </div>
                                    <div class="col-md-12">
                                        <span class="text-center">{{$expert->name}}</span>
                                    </div>
                                    <div class="col-md-12">
                                        <a href="{{route('experts.show' , $expert->id)}}" class="btn btn-dark">More
                                            info</a>
                                    </div>
                                </div>
                                @if($i % 2 ==0)
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            @endif
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop