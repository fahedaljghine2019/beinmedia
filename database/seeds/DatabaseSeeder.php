<?php

use App\Country;
use App\Expert;
use App\Specialization;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        //countries
        Country::create([
            'name' => 'Anabar'
        ]);

        $country = new Country();
        $country->fill(['name' => 'Syria']);
        $country->save();

        $country = new Country();
        $country->name = "Egypt";
        $country->save();


        //specializations
        Specialization::create([
            'name' => 'Doctor'
        ]);

        Specialization::create([
            'name' => 'Civil engineer'
        ]);

        Specialization::create([
            'name' => 'Computer Engineer'
        ]);

        //Experts seeds
        Expert::create([
            'name' => 'William Jordan',
            'timezone' => 'Pacific/Auckland',
            'start' => \Carbon\Carbon::createFromTime('6', '0', '0', 'Pacific/Auckland')->toTimeString(),
            'end' => '17:00:00',
            'specialization_id' => Specialization::where('name', 'Doctor')->first()->id,
            'country_id' => 1,
        ]);

        Expert::create([
            'name' => 'Quasi Shawa',
            'timezone' => 'Asia/Damascus',
            'start' => '6:00:00',
            'end' => '12:00:00',
            'specialization_id' => 2,
            'country_id' => 2,
        ]);

        Expert::create([
            'name' => 'Shimaa Badawy',
            'timezone' => 'Africa/Cairo',
            'start' => '13:00:00',
            'end' => '14:00:00',
            'specialization_id' => 3,
            'country_id' => 3,
        ]);


        //durations seed
        \App\Duration::create([
            'duration' => 15
        ]);

        \App\Duration::create([
            'duration' => 30
        ]);

        \App\Duration::create([
            'duration' => 45
        ]);

        \App\Duration::create([
            'duration' => 60
        ]);

        factory(\App\User::class, 10)->create();


        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
