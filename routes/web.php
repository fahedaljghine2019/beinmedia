<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/experts/{id}/show', 'HomeController@expert')->name('experts.show');
Route::get('/experts/{id}/book', 'HomeController@book')->name('experts.book');
Route::get('/experts', 'HomeController@experts')->name('experts');
Route::get('/slots/{id}', 'HomeController@slots')->name('slots');

Route::post('/book', 'HomeController@saveBooking')->name('book');
