<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'name'
    ];

    // I prefer this way because it reduces errors
    public function experts()
    {
        return $this->hasMany(Expert::class, "country_id");
    }
}
