<?php


use Carbon\Carbon;

class  Slot
{
    public $from;
    public $to;
    private $timezone;

    public function __construct($from, $to, $timezone)
    {
        $this->from = $from;
        $this->to = $to;
        $this->timezone = $timezone;
    }



    public function text()
    {
        return getTimeIn12($this->from) . " - " . getTimeIn12($this->to);
    }

    public function getSlotInTimezone($timezone)
    {
        $from = convertTimeFromTimezoneToAnother($this->from, $this->timezone, $timezone);
        $to = convertTimeFromTimezoneToAnother($this->to, $this->timezone, $timezone);

        return new Slot($from, $to, $timezone);
    }


    public function inCollection($collection)
    {
        foreach ($collection as $item) {
            if ($item->crossOver($this))
                return true;
        }

        return false;
    }


    public function crossOver($slot)
    {

        $this_from = Carbon::createFromFormat('H:i:s', $this->from);
        $this_to = Carbon::createFromFormat('H:i:s', $this->to);

        $slot_from = Carbon::createFromFormat('H:i:s', $slot->from);
        $slot_to = Carbon::createFromFormat('H:i:s', $slot->to);


        return ($this_from >= $slot_from && $this_from < $slot_to) ||
            ($slot_from > $this_from && $slot_from < $this_to);
    }


    public function isEqual($slot)
    {
        if ($this->from != $slot->from)
            return false;
        if ($this->to != $slot->to)
            return false;
        if ($this->timezone != $slot->timezone)
            return false;

        return true;
    }
}