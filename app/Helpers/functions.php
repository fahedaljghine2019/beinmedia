<?php

use Carbon\Carbon;

function getTimeZoneOffset($timezone)
{
    $dtz = new DateTimeZone($timezone);
    $time_in_timezone = new DateTime('now', $dtz);
    $offset = $dtz->getOffset($time_in_timezone) / 3600;

    $html = "(GMT";
    $html .= $offset > 0 ? " + " : " - ";
    $html .= $offset;
    $html .= ")";
    return $html;
}

function getDurationText($duration)
{
    if ($duration < 60)
        return $duration . " m";
    else {

        $rest = $duration % 60;
        if ($rest > 0)
            return $duration / 60 . " h " . $duration % 60 . " m";
        else
            return $duration / 60 . " h ";
    }
}

function convertTimeToSessionTimezone($time, $timezone)
{
    if (session()->has('timezone'))
        $session_timezone = session()->get('timezone');
    else
        $session_timezone = "Asia/Damascus";


    $time = Carbon::createFromFormat('H:i:s', $time, $timezone);
    $time->setTimezone($session_timezone);
    return $time->format('g:i A');

}

function convertTimeFromTimezoneToAnother($time, $source_timezone, $target_timezone)
{
    $time = Carbon::createFromFormat('H:i:s', $time, $source_timezone);
    $time->setTimezone($target_timezone);
    return $time->format('H:i:s');
}

function getTimeIn12($time)
{
    $time = Carbon::createFromFormat('H:i:s', $time);
    return $time->format('g:i A');
}