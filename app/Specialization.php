<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    protected $fillable = [
        'name'
    ];

    public function experts()
    {
        return $this->hasMany("App/Expert", "specialization_id" , "id");
    }
}
