<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Expert extends Model
{
    protected $fillable = [
        'name', 'timezone', 'start', 'end', 'specialization_id', 'country_id'
    ];


    public function getFromAttribute()
    {
        return convertTimeToSessionTimezone($this->start, $this->timezone);
    }

    public function getToAttribute()
    {
        return convertTimeToSessionTimezone($this->end, $this->timezone);

    }

    public function specialization()
    {
        return $this->belongsTo(Specialization::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
