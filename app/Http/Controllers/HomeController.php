<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Duration;
use App\Expert;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data["timezones"] = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        $ip = request()->ip();

        //while i am in local machine
        if ($ip == "127.0.0.1")
            $ip = "89.33.228.61";

        $ipInfo = file_get_contents('http://ip-api.com/json/' . $ip);
        $ipInfo = json_decode($ipInfo);


        //default timezone
        $timezone = "Asia/Damascus";

        if ($ipInfo->status == "success")
            $timezone = $ipInfo->timezone;

        if (session()->has('timezone')) {
            session()->forget('timezone');
        }

        session()->put('timezone', $timezone);

        $data["ip_timezone"] = $timezone;

        if (request()->has('timezone')) {

            if (session()->has('timezone')) {
                session()->forget('timezone');
            }

            session()->put('timezone', \request('timezone'));
        }


        return view('home', compact('data'));
    }

    public function experts()
    {
        $data["timezones"] = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        if (request()->has('timezone')) {

            if (session()->has('timezone')) {
                session()->forget('timezone');
            }

            session()->put('timezone', \request('timezone'));
        }


        $data["experts"] = Expert::all();

        return view('experts', compact('data'));
    }

    public function expert($id)
    {
        $data["timezones"] = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        if (request()->has('timezone')) {

            if (session()->has('timezone')) {
                session()->forget('timezone');
            }

            session()->put('timezone', \request('timezone'));
        }


        $data["expert"] = Expert::find($id);

        return view('expert', compact('data'));
    }

    public function book($id)
    {
        $data["timezones"] = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        if (request()->has('timezone')) {

            if (session()->has('timezone')) {
                session()->forget('timezone');
            }

            session()->put('timezone', \request('timezone'));
        }


        $data["expert"] = Expert::find($id);
        $data["durations"] = Duration::orderBy('duration')->get();
        return view('book', compact('data'));
    }

    public function saveBooking()
    {
        $appointment = new Appointment();
        $appointment->user_id = Auth::user()->id;
        $appointment->expert_id = \request('expert_id');
        $appointment->date = \request('date');
        $appointment->duration = \request('duration');
        $appointment->timezone = session()->get('timezone');


        $times = explode('-', \request('slot'));

        $appointment->start = Carbon::createFromFormat('H:i:s', trim($times[0]), $appointment->timezone)->format('H:i:s');
        $appointment->end = Carbon::createFromFormat('H:i:s', trim($times[1]), $appointment->timezone)->format('H:i:s');

        $appointment->save();

        session()->put('message', "Thank you for booking");
        session()->put('appointment', $appointment);
        return redirect()->route('home');
    }

    public function slots($id)
    {
        //get expert appointments
        $appointments = Appointment::where('expert_id', $id)->where('date', request('date'))->get();


        //get expert info
        $expert = Expert::find($id);

        $start = $expert->start;
        $end = $expert->end;
        $timezone = $expert->timezone;

        if (session()->has('timezone'))
            $session_timezone = session()->get('timezone');
        else
            $session_timezone = "Asia/Damascus";

        //get booked slots based on expert timezone
        $booked_slots = new Collection();
        foreach ($appointments as $appointment) {
            $slot = new \Slot($appointment->start, $appointment->end, $appointment->timezone);
            $booked_slots->push($slot->getSlotInTimezone($timezone));
        }


        //get available slots
        $shortest = Duration::orderBy('duration')->first()->duration;
        $expert_slots = new Collection();
        $curr = $start;
        $duration = \request('duration');
        do {
            $curr_end = Carbon::createFromFormat('H:i:s', $curr, $timezone)->addMinutes($duration)->format('H:i:s');
            $slot = new \Slot($curr, $curr_end, $timezone);
            if (!$slot->inCollection($booked_slots))
                $expert_slots->push($slot);
            $curr = Carbon::createFromFormat('H:i:s', $curr, $timezone)->addMinutes($shortest)->format('H:i:s');
        } while ($curr_end < $end);


        //convert slots to session timezone
        $session_slots = new Collection();
        foreach ($expert_slots as $slot) {
            $session_slots->push($slot->getSlotInTimezone($session_timezone));
        }

        return view('partials.slots', compact('session_slots'));
    }
}
