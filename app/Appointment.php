<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = [
        'date', 'start', 'end', 'duration', 'timezone', 'status', 'user_id', 'expert_id'
    ];


    protected $dates = [
        'date'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function expert()
    {
        return $this->belongsTo(Expert::class);
    }


}
